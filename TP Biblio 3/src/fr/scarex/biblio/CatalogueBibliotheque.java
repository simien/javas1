package fr.scarex.biblio;

import java.util.ArrayList;

/**
 * @author SCAREX
 * 
 */
public class CatalogueBibliotheque extends ArrayList<DocBibliotheque>
{
    private static final long serialVersionUID = 2698927958447376020L;

    public void show() {
        for (int i = 0; i < this.size(); i++) {
            System.out.println(i + " - " + this.get(i).getTitre());
        }
    }
    
    public boolean emprunt(int i, MembreBibliotheque m) {
        return this.get(i).emprunt(m);
    }
    
    public boolean reserver(int i, MembreBibliotheque m) {
        return this.get(i).reserver(m);
    }
    
    public boolean retour(int i) {
        return this.get(i).retour();
    }
    
    public boolean annuler(int i, MembreBibliotheque m) {
        return this.get(i).annulerReservation(m);
    }
}
