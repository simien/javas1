package fr.scarex.biblio;

/**
 * @author SCAREX
 *
 */
public class MembreBibliotheque
{
    private static int dernierNumeroAbonne = 0;
    protected String nom;
    protected String prenom;
    protected int tel;
    protected String adresse;
    protected final int numeroAbonne;
    
    public MembreBibliotheque(String nom, String prenom, int tel, String adresse) {
        this.nom = nom;
        this.prenom = prenom;
        this.tel = tel;
        this.adresse = adresse;
        this.numeroAbonne = dernierNumeroAbonne++;
    }

    @Override
    public String toString() {
        return String.format("MembreBibliotheque [nom=%s, prenom=%s, tel=%s, adresse=%s, numeroAbonne=%s]", nom, prenom, tel, adresse, numeroAbonne);
    }
    
    public String toScreenString() {
        return String.format("nom=%s, prenom=%s, tel=%s, adresse=%s, numeroAbonne=%s", nom, prenom, tel, adresse, numeroAbonne);
    }

    /**
     * @return the dernierNumeroAbonne
     */
    public static int getDernierNumeroAbonne() {
        return dernierNumeroAbonne;
    }

    /**
     * @param dernierNumeroAbonne the dernierNumeroAbonne to set
     */
    public static void setDernierNumeroAbonne(int dernierNumeroAbonne) {
        MembreBibliotheque.dernierNumeroAbonne = dernierNumeroAbonne;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the tel
     */
    public int getTel() {
        return tel;
    }

    /**
     * @param tel the tel to set
     */
    public void setTel(int tel) {
        this.tel = tel;
    }

    /**
     * @return the adresse
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * @param adresse the adresse to set
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * @return the numeroAbonne
     */
    public int getNumeroAbonne() {
        return numeroAbonne;
    }
}
