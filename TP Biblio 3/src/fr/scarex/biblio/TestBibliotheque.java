package fr.scarex.biblio;

import java.util.ArrayList;
import java.util.List;

/**
 * @author SCAREX
 * 
 */
public class TestBibliotheque
{
    public static CatalogueBibliotheque c = new CatalogueBibliotheque();
    public static List<MembreBibliotheque> membres = new ArrayList<>();

    public static void main(String[] args) {
        c.add(new DocBibliotheque("004.178.K20PM", "Introduction à Java", "J. Leblanc", 2015));
        c.add(new DocBibliotheque("967.4987.T2480", "Structure de Données", "M. Machin", 2017));
        
        membres.add(new MembreBibliotheque("Blettery", "Damien", 42, "6 rue VT"));
        membres.add(new MembreBibliotheque("Chaumet", "Simon", 21, "3 rue tajine"));

        System.out.println(c.get(0));
        c.emprunt(0, membres.get(0));
        System.out.println(c.get(0));
        c.reserver(0, membres.get(1));
        System.out.println(c.get(0));
        c.retour(0);
        System.out.println(c.get(0));
    }
}
