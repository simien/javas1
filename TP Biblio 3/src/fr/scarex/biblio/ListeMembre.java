package fr.scarex.biblio;

import java.util.ArrayList;

/**
 * @author SCAREX
 * 
 */
public class ListeMembre extends ArrayList<MembreBibliotheque>
{
    private static final long serialVersionUID = 2698927958447376020L;

    public void show() {
        for (int i = 0; i < this.size(); i++) {
            System.out.println(i + " - " + this.get(i).getNom() + " " + this.get(i).getPrenom());
        }
    }
}
