package fr.scarex.biblio;

/**
 * @author SCAREX
 *
 */
public class TestBibliotheque
{
    public static void main(String[] args) {
        DocBibliotheque d1 = new DocBibliotheque("004.178.K20PM", "Introduction à Java", "J. Leblanc", 2015);
        DocBibliotheque d2 = new DocBibliotheque("967.4987.T2480", "Structure de Données", "M. Machin", 2017);
        
        System.out.println(d1.getCodeArchivage());
        System.out.println(d1.getTitre());
        System.out.println(d1.getAuteur());
        System.out.println(d1.getAnnee());
        System.out.println("--------");
        System.out.println(d2.getCodeArchivage());
        System.out.println(d2.getTitre());
        System.out.println(d2.getAuteur());
        System.out.println(d2.getAnnee());
        System.out.println("--------");
        
        d1.setAnnee(2012);
        d2.setAuteur("M. Paul");
        
        System.out.println(d1.getAnnee());
        System.out.println(d2.getAuteur());
        
        System.out.println("--------");
        
        d1.emprunt();
        d1.reserver();
        d1.annulerReservation();
        d1.retour();
        d1.remiseEtagere();
        System.out.println(d1.isReserve());
        System.out.println(d1.getStatut());
    }
}
