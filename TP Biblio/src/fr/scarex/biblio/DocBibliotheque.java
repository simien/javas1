package fr.scarex.biblio;

/**
 * @author SCAREX
 * 
 */
public class DocBibliotheque
{
    protected String codeArchivage;
    protected String titre;
    protected String auteur;
    protected int annee;
    protected Statut statut = Statut.ETAGERE;
    protected boolean reserve = false;

    public DocBibliotheque(String codeArchivage, String titre, String auteur, int annee) {
        this.codeArchivage = codeArchivage;
        this.titre = titre;
        this.auteur = auteur;
        this.annee = annee;
    }

    /**
     * @return the codeArchivage
     */
    public String getCodeArchivage() {
        return codeArchivage;
    }

    /**
     * @return the titre
     */
    public String getTitre() {
        return titre;
    }

    /**
     * @return the auteur
     */
    public String getAuteur() {
        return auteur;
    }

    /**
     * @param codeArchivage
     *            the codeArchivage to set
     */
    public void setCodeArchivage(String codeArchivage) {
        this.codeArchivage = codeArchivage;
    }

    /**
     * @param titre
     *            the titre to set
     */
    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     * @param auteur
     *            the auteur to set
     */
    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    /**
     * @return the annee
     */
    public int getAnnee() {
        return annee;
    }

    /**
     * @param annee
     *            the annee to set
     */
    public void setAnnee(int annee) {
        this.annee = annee;
    }

    /**
     * @return the statut
     */
    public Statut getStatut() {
        return statut;
    }

    /**
     * @param statut
     *            the statut to set
     */
    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    /**
     * @return the reserve
     */
    public boolean isReserve() {
        return reserve;
    }

    /**
     * @param reserve
     *            the reserve to set
     */
    public void setReserve(boolean reserve) {
        this.reserve = reserve;
    }

    public boolean emprunt() {
        if (this.statut == Statut.ETAGERE || this.statut == Statut.RESERVATION) {
            this.statut = Statut.EMPRUNTE;
            return true;
        }
        return false;
    }

    public boolean reserver() {
        if (!this.reserve && this.statut == Statut.EMPRUNTE) {
            this.reserve = true;
            return true;
        }
        return false;
    }

    public boolean retour() {
        if (this.statut == Statut.EMPRUNTE && this.reserve) {
            this.statut = Statut.RESERVATION;
            return true;
        } else if (this.statut == Statut.EMPRUNTE) {
            this.statut = Statut.PILE_RETOUR;
            return true;
        }
        return false;
    }

    public boolean annulerReservation() {
        if (this.statut == Statut.RESERVATION) {
            this.statut = Statut.PILE_RETOUR;
            this.reserve = false;
            return true;
        } else if (this.statut == Statut.EMPRUNTE) {
            this.reserve = false;
            return true;
        }
        return false;
    }

    public boolean remiseEtagere() {
        if (this.statut == Statut.PILE_RETOUR) {
            this.statut = Statut.ETAGERE;
            return true;
        }
        return false;
    }

    public static enum Statut
    {
        EMPRUNTE,
        ETAGERE,
        RESERVATION,
        PILE_RETOUR;
    }
}
