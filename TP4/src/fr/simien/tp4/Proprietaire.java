package fr.simien.tp4;

/**
 * @author SCAREX
 *
 */
public class Proprietaire
{
    private String prenom;
    private String nom;
    private String adresse;
    private int taxeFonciere;
    private int taxeHabitation;
    
    public Proprietaire(String prenom, String nom, String adresse, int taxeFonciere, int taxeHabitation) {
        this.prenom = prenom;
        this.nom = nom;
        this.adresse = adresse;
        this.taxeFonciere = taxeFonciere;
        this.taxeHabitation = taxeHabitation;
    }

    public String toString() {
        return "Proprietaire [prenom=" + prenom + ", nom=" + nom + ", adresse=" + adresse + ", taxeFonciere=" + taxeFonciere + ", taxeHabitation=" + taxeHabitation + "]";
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the adresse
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * @param adresse the adresse to set
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * @return the taxeFonciere
     */
    public int getTaxeFonciere() {
        return taxeFonciere;
    }

    /**
     * @param taxeFonciere the taxeFonciere to set
     */
    public void setTaxeFonciere(int taxeFonciere) {
        this.taxeFonciere = taxeFonciere;
    }

    /**
     * @return the taxeHabitation
     */
    public int getTaxeHabitation() {
        return taxeHabitation;
    }

    /**
     * @param taxeHabitation the taxeHabitation to set
     */
    public void setTaxeHabitation(int taxeHabitation) {
        this.taxeHabitation = taxeHabitation;
    }
}
