package fr.simien.tp4;


/**
 * @author SCAREX
 * 
 */
public class BienImmobilier
{
    private static int refindex = 0;
    private final int ref;
    private double price;
    private boolean loc;
    private boolean sell;
    private Proprietaire prop;
    private Locataire locobj;

    public BienImmobilier(double price, boolean loc, boolean sell, Proprietaire propname, Locataire locname) {
        this.ref = BienImmobilier.getNextIndex();
        this.price = price;
        this.loc = loc;
        this.sell = sell;
        this.prop = propname;
        this.locobj = locname;
    }

    private static int getNextIndex() {
        return BienImmobilier.refindex++;
    }

    /**
     * @return the loc
     */
    public boolean estEnLocation() {
        return loc;
    }

    /**
     * @return the sell
     */
    public boolean estEnVente() {
        return sell;
    }

    /**
     * @param locname
     *            the locname to set
     */
    public void location(Locataire locname) {
        this.locobj = locname;
    }

    public void finLocation() {
        this.loc = false;
        this.locobj = null;
    }

    public void vente(double prixPropose, Proprietaire nomProprietaire) {
        this.price = prixPropose;
        this.prop = nomProprietaire;
    }

    /**
     * @return the refindex
     */
    public static int getRefindex() {
        return refindex;
    }

    /**
     * @param refindex
     *            the refindex to set
     */
    public static void setRefindex(int refindex) {
        BienImmobilier.refindex = refindex;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the loc
     */
    public boolean isLoc() {
        return loc;
    }

    /**
     * @param loc
     *            the loc to set
     */
    public void setLoc(boolean loc) {
        this.loc = loc;
    }

    /**
     * @return the sell
     */
    public boolean isSell() {
        return sell;
    }

    /**
     * @param sell
     *            the sell to set
     */
    public void setSell(boolean sell) {
        this.sell = sell;
    }

    /**
     * @return the propname
     */
    public Proprietaire getPropname() {
        return prop;
    }

    /**
     * @param propname
     *            the propname to set
     */
    public void setPropname(Proprietaire propname) {
        this.prop = propname;
    }

    /**
     * @return the locname
     */
    public Locataire getLocname() {
        return locobj;
    }

    /**
     * @param locname
     *            the locname to set
     */
    public void setLocname(Locataire locname) {
        this.locobj = locname;
    }

    /**
     * @return the ref
     */
    public int getRef() {
        return ref;
    }

    public String toString() {
        return "Bien immobilier de ref : " + ref + ", de prix : " + price + ", location : " + loc + ", en vente : " + sell + ", propriétaire : " + prop + ", locataire : " + locobj;
    }
}
