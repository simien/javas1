package fr.simien.tp4;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author SCAREX
 * 
 */
public class TP4Test
{
    private static Scanner sc;
    private static List<BienImmobilier> immos = new ArrayList<>();
    private static List<Proprietaire> props = new ArrayList<>();
    private static List<Locataire> locs = new ArrayList<>();

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        menuP();
        sc.close();
    }

    public static void menuP() {
        int i = 0;
        do {
            System.out.println("-----------------------------------------");
            System.out.println("1  - Créer un nouveau propriétaire");
            System.out.println("2  - Créer un nouveau locataire");
            System.out.println("3  - Créer un nouveau bien immobilier");
            System.out.println("4  - Afficher les propriétaires");
            System.out.println("5  - Afficher les locataires");
            System.out.println("6  - Afficher les biens immobiliers");
            System.out.println("7  - Modifier propriétaire");
            System.out.println("8  - Modifier locataire");
            System.out.println("9  - Modifier bien immobilier");
            System.out.println("10 - Quitter");

            i = sc.nextInt();
            sc.nextLine();

            switch (i) {
            case 1:
                newProp();
                break;
            case 2:
                newLoc();
                break;
            case 3:
                newImmo();
                break;
            case 4:
                showProps();
                break;
            case 5:
                showLocs();
                break;
            case 6:
                showImmos();
                break;
            case 7:
                modifyProp();
                break;
            }
        } while (i != 10);
    }

    public static void newProp() {
        System.out.println("Entrez le nom");
        String n = sc.nextLine();
        System.out.println("Entrez le prénom");
        String p = sc.nextLine();
        System.out.println("Entrez l'adresse");
        String a = sc.nextLine();
        System.out.println("Entrez le montant de la taxe foncière");
        int tf = sc.nextInt();
        sc.nextLine();
        System.out.println("Entrez le montant de la taxe d'habitation");
        int th = sc.nextInt();
        sc.nextLine();
        Proprietaire prop = new Proprietaire(p, n, a, tf, th);
        props.add(prop);
    }

    public static void newLoc() {
        System.out.println("Entrez le nom");
        String n = sc.nextLine();
        System.out.println("Entrez le prénom");
        String p = sc.nextLine();
        System.out.println("Entrez l'adresse");
        String a = sc.nextLine();
        System.out.println("Entrez le montant du loyer");
        int l = sc.nextInt();
        sc.nextLine();
        System.out.println("Entrez le montant de la taxe d'habitation");
        int th = sc.nextInt();
        sc.nextLine();
        Locataire loc = new Locataire(p, n, a, l, th);
        locs.add(loc);
    }

    public static void newImmo() {
        System.out.println("Entrez le prix");
        double p = sc.nextDouble();
        sc.nextLine();
        System.out.println("Choisissez un propriétaire :");
        showProps();
        int i = sc.nextInt();
        sc.nextLine();
        BienImmobilier b = new BienImmobilier(p, false, false, props.get(i), null);
        immos.add(b);
    }

    public static void showProps() {
        for (int i = 0; i < props.size(); i++) {
            System.out.println(i + " - " + props.get(i));
        }
    }

    public static void showLocs() {
        for (int i = 0; i < locs.size(); i++) {
            System.out.println(i + " - " + locs.get(i));
        }
    }

    public static void showImmos() {
        for (int i = 0; i < immos.size(); i++) {
            System.out.println(i + " - " + immos.get(i));
        }
    }

    public static void modifyProp() {
        System.out.println("Choisissez un propriétaire :");
        showProps();
        int ip = sc.nextInt();
        sc.nextLine();
        Proprietaire p = props.get(ip);

        System.out.println("Choisissez ce que vous voulez modifier :");
        System.out.println("1 - Prénom");
        System.out.println("2 - Nom");
        System.out.println("3 - Adresse");
        System.out.println("4 - Taxe foncière");
        System.out.println("5 - Taxe habitation");
        int iv = sc.nextInt();
        sc.nextLine();

        switch (iv) {
        case 1:
            System.out.println("Entrez le nouveau prénom");
            p.setPrenom(sc.nextLine());
            break;
        case 2:
            System.out.println("Entrez le nouveau nom");
            p.setNom(sc.nextLine());
            break;
        case 3:
            System.out.println("Entrez la nouvelle adresse");
            p.setAdresse(sc.nextLine());
            break;
        case 4:
            System.out.println("Entrez la nouvelle taxe foncière");
            p.setTaxeFonciere(sc.nextInt());
            sc.nextLine();
            break;
        case 5:
            System.out.println("Entrez la nouvelle taxe d'habitation");
            p.setTaxeHabitation(sc.nextInt());
            sc.nextLine();
            break;
        }
    }

    public static void modifyLocs() {
        System.out.println("Choisissez un locataire :");
        showLocs();
        int il = sc.nextInt();
        sc.nextLine();
        Locataire l = locs.get(il);
        System.out.println("Choisissez ce que vous voulez modifier :");
        System.out.println("1 - Prénom");
        System.out.println("2 - Nom");
        System.out.println("3 - Adresse");
        System.out.println("4 - Loyer");
        System.out.println("5 - Taxe habitation");
        int iv = sc.nextInt();
        sc.nextLine();

        switch (iv) {
        case 1:
            System.out.println("Entrez le nouveau prénom");
            l.setPrenom(sc.nextLine());
            break;
        case 2:
            System.out.println("Entrez le nouveau nom");
            l.setNom(sc.nextLine());
            break;
        case 3:
            System.out.println("Entrez la nouvelle adresse");
            l.setAdresse(sc.nextLine());
            break;
        case 4:
            System.out.println("Entrez le nouveau loyer");
            l.setLoyer(sc.nextInt());
            sc.nextLine();
            break;
        case 5:
            System.out.println("Entrez la nouvelle taxe d'habitation");
            l.setTaxeHabitation(sc.nextInt());
            sc.nextLine();
            break;
        }
    }

    public static void modifyImmos() {
        System.out.println("Choisissez un bien immobilier :");
        showImmos();
        int iI = sc.nextInt();
        sc.nextLine();
        BienImmobilier i = immos.get(iI);
        System.out.println("Choisissez ce que vous voulez modifier :");
        System.out.println("1 - Prix");
        System.out.println("2 - Location");
        System.out.println("3 - Vente");
        System.out.println("4 - Propriétaire");
        System.out.println("5 - Locataire");
        int iv = sc.nextInt();
        sc.nextLine();
        
        switch (iv) {
        case 1:
            System.out.println("Entrez le nouveau prix");
            i.setPrice(sc.nextDouble());
            sc.nextLine();
            break;
        case 2:
            System.out.println("Entrez le nouveau statut de location");
            i.setLoc(sc.nextBoolean());
            sc.nextLine();
            break;
        case 3:
            System.out.println("Entrez le nouveau statut de vente");
            i.setSell(sc.nextBoolean());
            sc.nextLine();
            break;
        case 4:
            System.out.println("Entrez le nouveau propriétaire");
            showProps();
            int ip = sc.nextInt();
            sc.nextLine();
            i.setPropname(props.get(ip));
            break;
        case 5:
            System.out.println("Entrez le nouveau locataire");
            showLocs();
            int il = sc.nextInt();
            sc.nextLine();
            i.setPropname(props.get(il));
            break;
        }
    }
}
