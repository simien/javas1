package fr.simien.calcidalog;

import javax.swing.JOptionPane;

/**
 * @author SCAREX
 * 
 */
public class CalcDialog
{
    public static void main(String[] args) {
        int i1 = Integer.parseInt(JOptionPane.showInputDialog(null, "Entrez le premier nombre", JOptionPane.INFORMATION_MESSAGE));
        char c = JOptionPane.showInputDialog(null, "Entrez l'opérateur").charAt(0);
        int i2 = Integer.parseInt(JOptionPane.showInputDialog(null, "Entrez le deuxième nombre nombre", JOptionPane.INFORMATION_MESSAGE));

        double res = 0;
        switch (c) {
        case '+':
            res = i1 + i2;
            break;
        case '-':
            res = i1 - i2;
            break;
        case '*':
        case 'x':
            res = i1 * i2;
            break;
        case '/':
            res = ((double) i1) / ((double) i2);
            break;
        }

        JOptionPane.showMessageDialog(null, "Le résultat est : " + res);
    }
}
