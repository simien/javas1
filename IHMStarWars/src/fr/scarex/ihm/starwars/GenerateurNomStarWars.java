package fr.scarex.ihm.starwars;

import javax.swing.JOptionPane;

/**
 * @author SCAREX
 * 
 */
public class GenerateurNomStarWars
{
    public static void main(String[] args) {
        showJDialog();
    }

    private static void showJDialog() {
        String pren = JOptionPane.showInputDialog(null, "Entrez votre prénom", JOptionPane.INFORMATION_MESSAGE);
        String nom = JOptionPane.showInputDialog(null, "Entrez votre nom de famille", JOptionPane.INFORMATION_MESSAGE);
        String nomMaman = JOptionPane.showInputDialog(null, "Entrez le nom de jeune fille de votre mère", JOptionPane.INFORMATION_MESSAGE);
        String ville = JOptionPane.showInputDialog(null, "Entrez votre ville de naissance", JOptionPane.INFORMATION_MESSAGE);

        String starPren = nom.substring(0, 3) + pren.substring(0, 2).toLowerCase();
        String starNom = nomMaman.substring(0, 2) + ville.substring(0, 3).toLowerCase();

        JOptionPane.showMessageDialog(null, "Votre nom StarWars est : " + starPren + " " + starNom);
    }
}
