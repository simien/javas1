package fr.scarex.biblio;

/**
 * @author SCAREX
 *
 */
public class TestBibliotheque
{
    public static void main(String[] args) {
        DocBibliotheque d1 = new DocBibliotheque("004.178.K20PM", "Introduction à Java", "J. Leblanc", 2015);
        DocBibliotheque d2 = new DocBibliotheque("967.4987.T2480", "Structure de Données", "M. Machin", 2017);
        
        MembreBibliotheque m = new MembreBibliotheque("CHAUMET", "Simon", 789597755, "3 rue antoine de st exupery - 01800 meximieux");
        MembreBibliotheque m1 = new MembreBibliotheque("BLETTERY", "Damien", 42, "86 rue jacqueline - 69100 Villeurbanne");
        
        System.out.println(d1.getCodeArchivage());
        System.out.println(d1.getTitre());
        System.out.println(d1.getAuteur());
        System.out.println(d1.getAnnee());
        System.out.println("--------");
        System.out.println(d2.getCodeArchivage());
        System.out.println(d2.getTitre());
        System.out.println(d2.getAuteur());
        System.out.println(d2.getAnnee());
        System.out.println("--------");
        
        d1.setAnnee(2012);
        d2.setAuteur("M. Paul");
        
        System.out.println(d1.getAnnee());
        System.out.println(d2.getAuteur());
        
        System.out.println("--------");
        
        d1.emprunt(m);
        d2.emprunt(m1);
        d2.reserver(m);
        
        System.out.println("Emprunteur doc 1 : " + d1.getEmprunteur());
        System.out.println("Emprunteur doc 2 : " + d2.getEmprunteur()  + " | Reservation : " + d2.getReserveur());

        for (DocBibliotheque.Statut s : DocBibliotheque.Statut.values()) {
            System.out.println(s + " : " + s.getCount());
        }
        
        System.out.println("--------");

        d2.retour();
        d2.reservationRecup();
        System.out.println("Emprunteur doc 2 : " + d2.getEmprunteur()  + " | Reservation : " + d2.getReserveur());
        for (DocBibliotheque.Statut s : DocBibliotheque.Statut.values()) {
            System.out.println(s + " : " + s.getCount());
        }
        
        System.out.println("--------");
        
        d2.retour();
        d2.remiseEtagere();
        for (DocBibliotheque.Statut s : DocBibliotheque.Statut.values()) {
            System.out.println(s + " : " + s.getCount());
        }
    }
}
