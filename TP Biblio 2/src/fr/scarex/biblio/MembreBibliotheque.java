package fr.scarex.biblio;

/**
 * @author SCAREX
 *
 */
public class MembreBibliotheque
{
    private static int dernierNumeroAbonne = 0;
    protected String nom;
    protected String prenom;
    protected int tel;
    protected String adresse;
    protected final int numeroAbonne;
    
    public MembreBibliotheque(String nom, String prenom, int tel, String adresse) {
        this.nom = nom;
        this.prenom = prenom;
        this.tel = tel;
        this.adresse = adresse;
        this.numeroAbonne = dernierNumeroAbonne++;
    }

    @Override
    public String toString() {
        return String.format("MembreBibliotheque [nom=%s, prenom=%s, tel=%s, adresse=%s, numeroAbonne=%s]", nom, prenom, tel, adresse, numeroAbonne);
    }
}
