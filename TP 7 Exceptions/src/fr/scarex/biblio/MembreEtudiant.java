package fr.scarex.biblio;

/**
 * @author SCAREX
 * 
 */
public class MembreEtudiant extends MembreBibliotheque
{
    public MembreEtudiant(String nom, String prenom, int tel, String adresse) {
        super(nom, prenom, tel, adresse);
    }

    @Override
    public boolean peutEmprunterAutreDocument() {
        return this.nbEmprunts < 4;
    }
}
