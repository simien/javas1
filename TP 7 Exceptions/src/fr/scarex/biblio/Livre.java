package fr.scarex.biblio;

/**
 * @author SCAREX
 *
 */
public class Livre extends DocBibliotheque
{
    private String isbn;
    private int nbPages;
    private String editeur;
    protected String auteur;
    
    public Livre(String codeArchivage, String titre, String auteur, int annee, String isbn, int nbPages, String editeur) {
        super(codeArchivage, titre, annee);
        this.isbn = isbn;
        this.nbPages = nbPages;
        this.editeur = editeur;
    }

    /**
     * @return the isbn
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * @param isbn the isbn to set
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    /**
     * @return the nbPages
     */
    public int getNbPages() {
        return nbPages;
    }

    /**
     * @param nbPages the nbPages to set
     */
    public void setNbPages(int nbPages) {
        this.nbPages = nbPages;
    }

    /**
     * @return the editeur
     */
    public String getEditeur() {
        return editeur;
    }

    /**
     * @param editeur the editeur to set
     */
    public void setEditeur(String editeur) {
        this.editeur = editeur;
    }

    /**
     * @return the auteur
     */
    public String getAuteur() {
        return auteur;
    }

    /**
     * @param auteur the auteur to set
     */
    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    @Override
    public String getCreateur() {
        return this.getAuteur();
    }

    @Override
    public void setCreateur(String createur) {
        this.setAuteur(createur);
    }

    @Override
    public boolean empruntable() {
        return true;
    }

    @Override
    public String toScreenString() {
        return String.format("Livre : isbn=%s, nbPages=%s, editeur=%s, auteur=%s", isbn, nbPages, editeur, auteur);
    }
}
