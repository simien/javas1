package fr.scarex.biblio;

import fr.scarex.biblio.exception.AnnulReservException;
import fr.scarex.biblio.exception.EmpruntException;
import fr.scarex.biblio.exception.ReserveException;
import fr.scarex.biblio.exception.RetourException;

/**
 * @author SCAREX
 * 
 */
public abstract class DocBibliotheque
{
    protected String codeArchivage;
    protected String titre;
    protected int annee;
    protected Statut statut = Statut.ETAGERE;
    protected MembreBibliotheque reserveur = null;
    protected MembreBibliotheque emprunteur = null;

    public DocBibliotheque(String codeArchivage, String titre, int annee) {
        this.codeArchivage = codeArchivage;
        this.titre = titre;
        this.annee = annee;
    }

    /**
     * @return the codeArchivage
     */
    public String getCodeArchivage() {
        return codeArchivage;
    }

    /**
     * @return the titre
     */
    public String getTitre() {
        return titre;
    }

    /**
     * @return the auteur
     */
    public abstract String getCreateur();

    /**
     * @param codeArchivage
     *            the codeArchivage to set
     */
    public void setCodeArchivage(String codeArchivage) {
        this.codeArchivage = codeArchivage;
    }

    /**
     * @param titre
     *            the titre to set
     */
    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     * @param auteur
     *            the auteur to set
     */
    public abstract void setCreateur(String createur);

    /**
     * @return the annee
     */
    public int getAnnee() {
        return annee;
    }

    /**
     * @param annee
     *            the annee to set
     */
    public void setAnnee(int annee) {
        this.annee = annee;
    }

    /**
     * @return the statut
     */
    public Statut getStatut() {
        return statut;
    }

    /**
     * @param statut
     *            the statut to set
     */
    public void setStatut(Statut statut) {
        this.statut.decr();
        statut.incr();
        this.statut = statut;
    }

    /**
     * @return the reserve
     */
    public MembreBibliotheque getReserveur() {
        return reserveur;
    }

    /**
     * @param reserve
     *            the reserve to set
     */
    public void setReserveur(MembreBibliotheque reserve) {
        this.reserveur = reserve;
    }

    /**
     * @return the emprunteur
     */
    public MembreBibliotheque getEmprunteur() {
        return emprunteur;
    }

    /**
     * @param emprunteur
     *            the emprunteur to set
     */
    public void setEmprunteur(MembreBibliotheque emprunteur) {
        this.emprunteur = emprunteur;
    }

    public boolean emprunt(MembreBibliotheque m) throws EmpruntException {
        if (this.empruntable() && this.statut == Statut.ETAGERE) {
            this.setStatut(Statut.EMPRUNTE);
            this.emprunteur = m;
            return true;
        } else if (this.empruntable() && this.statut == Statut.RESERVATION && m == this.reserveur) {
            this.emprunteur = this.reserveur;
            this.reserveur = null;
            this.setStatut(Statut.EMPRUNTE);
            return true;
        }
        throw new EmpruntException();
    }

    public boolean reserver(MembreBibliotheque m) {
        if (this.empruntable() && this.reserveur == null && this.statut == Statut.EMPRUNTE) {
            this.reserveur = m;
            return true;
        }
        throw new ReserveException();
    }

    public boolean retour() {
        if (this.empruntable() && this.statut == Statut.EMPRUNTE && this.reserveur != null) {
            this.setStatut(Statut.RESERVATION);
            this.emprunteur = null;
            return true;
        } else if (this.empruntable() && this.statut == Statut.EMPRUNTE) {
            this.setStatut(Statut.PILE_RETOUR);
            this.emprunteur = null;
            return true;
        }
        throw new RetourException();
    }

    public boolean annulerReservation(MembreBibliotheque m) {
        if (this.reserveur == m) {
            if (this.statut == Statut.RESERVATION) {
                this.setStatut(Statut.PILE_RETOUR);
                this.reserveur = null;
                return true;
            } else if (this.statut == Statut.EMPRUNTE) {
                this.reserveur = null;
                return true;
            }
        }
        throw new AnnulReservException();
    }

    public boolean remiseEtagere() {
        if (this.statut == Statut.PILE_RETOUR) {
            this.setStatut(Statut.ETAGERE);
            return true;
        }
        return false;
    }
    
    public abstract boolean empruntable();

    @Override
    public String toString() {
        return String.format("DocBibliotheque [codeArchivage=%s, titre=%s, annee=%s, statut=%s, reserveur=%s, emprunteur=%s]", codeArchivage, titre, annee, statut, reserveur, emprunteur);
    }

    public String toScreenString() {
        return String.format("codeArchivage=%s, titre=%s, annee=%s, statut=%s, reserveur=%s, emprunteur=%s", codeArchivage, titre, annee, statut, reserveur != null, emprunteur != null);
    }

    public static enum Statut
    {
        EMPRUNTE,
        ETAGERE,
        RESERVATION,
        PILE_RETOUR;

        private int count = 0;

        public void decr() {
            if (count > 0) count--;
        }

        public void incr() {
            count++;
        }

        public int getCount() {
            return count;
        }
    }
}
