package fr.scarex.biblio;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * @author SCAREX
 *
 */
public class TestPerf
{
    private static DocBibliotheque[] docs = new DocBibliotheque[10000];
    
    public static void main(String[] args) {
        for (int i = 0; i < 10000; i++) {
            docs[i] = new CD("" + i,"" + i,"" + i, i, new ArrayList<String>());
        }
        
        //testTreeMap();
        testArrayList();
        testLinkedList();
        testHashset();
    }

    private static void testArrayList() {
        long t = System.currentTimeMillis();
        ArrayList<DocBibliotheque> l = new ArrayList<>();
        for (DocBibliotheque d : docs) {
            l.add(d);
        }
        System.out.println("Temps ajout ArrayList : " + (System.currentTimeMillis() - t));
        
        t = System.currentTimeMillis();
        for (DocBibliotheque d : docs) {
            l.contains(d);
        }
        System.out.println("Temps contains ArrayList : " + (System.currentTimeMillis() - t));
    }
    
    private static void testLinkedList() {
        long t = System.currentTimeMillis();
        LinkedList<DocBibliotheque> l = new LinkedList<>();
        for (DocBibliotheque d : docs) {
            l.add(d);
        }
        System.out.println("Temps ajout LinkedList : " + (System.currentTimeMillis() - t));
        
        t = System.currentTimeMillis();
        for (DocBibliotheque d : docs) {
            l.contains(d);
        }
        System.out.println("Temps contains LinkedList : " + (System.currentTimeMillis() - t));
    }
    
    private static void testHashset() {
        long t = System.currentTimeMillis();
        HashSet<DocBibliotheque> l = new HashSet<>();
        for (DocBibliotheque d : docs) {
            l.add(d);
        }
        System.out.println("Temps ajout HashSet : " + (System.currentTimeMillis() - t));
        
        t = System.currentTimeMillis();
        for (DocBibliotheque d : docs) {
            l.contains(d);
        }
        System.out.println("Temps contains HashSet : " + (System.currentTimeMillis() - t));
    }
    
    private static void testTreeSet() {
        TreeSet<DocBibliotheque> l = new TreeSet<>();
        for (DocBibliotheque d : docs) {
            l.add(d);
        }
        
        for (DocBibliotheque d : l) {
            System.out.println(d.toScreenString());
        }
    }

    private static void testTreeMap() {
        TreeMap<String, DocBibliotheque> map = new TreeMap<>();
        for (DocBibliotheque d : docs) {
            map.put(d.getCodeArchivage(), d);
        }
        
        Set<Entry<String, DocBibliotheque>> s = map.entrySet();
        Iterator<Entry<String, DocBibliotheque>> it = s.iterator();
        while (it.hasNext()) {
            Entry<String, DocBibliotheque> e = it.next();
            System.out.println(e.getValue().toScreenString());
        }
    }
}
