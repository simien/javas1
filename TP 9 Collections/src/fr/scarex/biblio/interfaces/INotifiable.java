package fr.scarex.biblio.interfaces;

import fr.scarex.biblio.DocBibliotheque;

/**
 * @author SCAREX
 *
 */
public interface INotifiable
{
    public void docDisponible(DocBibliotheque d);
}
