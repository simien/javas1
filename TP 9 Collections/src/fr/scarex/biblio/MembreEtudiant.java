package fr.scarex.biblio;

import java.util.Scanner;

import fr.scarex.biblio.interfaces.IEmpruntable;

/**
 * @author SCAREX
 * 
 */
public class MembreEtudiant extends MembreBibliotheque
{
    public MembreEtudiant(String nom, String prenom, int tel, String adresse) {
        super(nom, prenom, tel, adresse);
    }

    @Override
    public boolean peutEmprunterAutreDocument() {
        return this.nbEmprunts < 4;
    }

    @Override
    public void docDisponible(DocBibliotheque d) {
        System.out.println("Le document " + d.titre + " qui a été réservé par le membre du personnel " + this.nom + " " + this.prenom + " est désormais disponible à l’emprunt au bureau des réservations.");
        System.out.println("Voulez-vous l'emprunter maintenant ? O/N");

        Scanner sc = new Scanner(System.in);
        String s = null;
        do {
            s = sc.nextLine();
            if (s.startsWith("O")) {
                if (d instanceof IEmpruntable) ((IEmpruntable) d).emprunt(this);
                break;
            } else if (s.startsWith("N")) {
                break;
            }
        } while (true);
        sc.close();
    }
}
