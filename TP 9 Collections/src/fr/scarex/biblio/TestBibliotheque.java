package fr.scarex.biblio;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * @author SCAREX
 * 
 */
public class TestBibliotheque
{
    public static CatalogueBibliotheque docs = new CatalogueBibliotheque();
    public static List<MembreBibliotheque> membres = new ArrayList<>();
    public static List<EmployeBibliotheque> emps = new ArrayList<>();
    private static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);

        try {
            docs.add(new DocURL("ef", "zefz", 2000, "zefze", new URL("http://www.google.fr"), "google"));
            docs.add(new Livre("AEGN", "zgsethj", "sqfzgg", 2000, "zegzhehjjn", 12, "tejetje"));
        } catch (MalformedURLException e) {}

        creerMembreEtudiant("Chaumet", "Simon", 42, "zefg");
        creerMembreEtudiant("Blettery", "Damien", 12, "zqghqeh");
        creerMembrePersonnel("Sébastien", "Patrick", 24, "rhiozko,rz");
        creerEmployerBibliotheque("abcd", "Jean-Paul");

        mainMenu();
    }

    public static void mainMenu() {
        System.out.println("----------------");
        System.out.println("1 - Documents");
        System.out.println("2 - Membres");
        System.out.println("3 - État bibliothèque");

        int k = getInputInt();
        switch (k) {
        case 1:
            menuDocs();
            break;
        case 2:
            menuMembres();
            break;
        case 3:
            menuEtatBiblio();
            break;
        default:
            mainMenu();
        }
    }

    private static void menuDocs() {
        System.out.println("----------------");
        System.out.println("1 - Ajouter livre");
        System.out.println("2 - Ajouter cd");
        System.out.println("3 - Ajouter site");
        System.out.println("4 - Voir documents");
        System.out.println("5 - Retour");

        int k = getInputInt(1, 5);
        switch (k) {
        case 1:
            menuAjouterLivre();
            break;
        case 2:
            menuAjouterCD();
            break;
        case 3:
            menuAjouterSite();
            break;
        case 4:
            menuListeDoc();
            break;
        case 5:
            mainMenu();
            break;
        }
    }

    private static void menuMembres() {
        System.out.println("----------------");
        System.out.println("1 - Ajouter membre");
        System.out.println("2 - Ajouter employé");
        System.out.println("3 - Liste membres");
        System.out.println("4 - Liste employés");
        System.out.println("5 - Retour");

        int k = getInputInt(1, 5);
        switch (k) {
        case 1:
            ajouterMembre();
            break;
        case 2:
            ajouterEmploye();
            break;
        case 3:
            listeMembres();
            break;
        case 4:
            listeEmployes();
            break;
        case 5:
            mainMenu();
        }
    }

    private static void ajouterEmploye() {
        System.out.println("----------------");
        System.out.println("Entrez l'id");
        String id = sc.nextLine();

        System.out.println("Entrez le nom");
        String nom = sc.nextLine();

        creerEmployerBibliotheque(id, nom);
        menuMembres();
    }

    private static void ajouterMembre() {
        System.out.println("----------------");
        System.out.println("Entrez le nom");
        String nom = sc.nextLine();

        System.out.println("Entrez le prenom");
        String pren = sc.nextLine();

        System.out.println("Entrez le numero de telephone");
        int tel = getInputInt(0, Integer.MAX_VALUE);

        System.out.println("Entrez l'adresse");
        String adr = sc.nextLine();

        System.out.println("Entrez le type");
        System.out.println("1 - Etudiant");
        System.out.println("2 - Personnel");
        int type = getInputInt(1, 2);

        if (type == 1) {
            creerMembreEtudiant(nom, pren, tel, adr);
        } else if (type == 2) {
            creerMembrePersonnel(nom, pren, tel, adr);
        }

        menuMembres();
    }

    private static void listeMembres() {
        int k = menuMembre();
        if (k != -1) {
            System.out.println("----------------");
            System.out.println("1 - Supprimer");
            System.out.println("2 - Voir emprunts");
            System.out.println("3 - Retour");

            int l = getInputInt(1, 3);
            switch (l) {
            case 1:
                membres.remove(l);
                System.out.println("membre supprimé");
                break;
            case 2:
                menuEmprunts(k);
                break;
            case 3:
                menuMembres();
            }
        }
        menuMembres();
    }

    private static void listeEmployes() {
        int k = menuEmploye();
        if (k != -1) {
            System.out.println("----------------");
            System.out.println("1 - Supprimer");
            System.out.println("2 - Retour");

            int l = getInputInt(1, 2);
            switch (l) {
            case 1:
                emps.remove(l);
                System.out.println("membre supprimé");
                break;
            case 2:
                menuMembres();
            }
        }
        menuMembres();
    }

    private static void menuEmprunts(int l) {
        System.out.println("----------------");
        MembreBibliotheque m = membres.get(l);
        for (int i = 0; i < docs.size(); i++) {
            if (docs.get(i).getEmprunteur() != null && docs.get(i).getEmprunteur().equals(m)) {
                System.out.println(docs.get(i));
            }
        }
        listeMembres();
    }

    private static void menuEtatBiblio() {
        System.out.println("----------------");
        System.out.println(DocBibliotheque.Statut.EMPRUNTE + " : " + DocBibliotheque.Statut.EMPRUNTE.getCount());
        System.out.println(DocBibliotheque.Statut.ETAGERE + " : " + DocBibliotheque.Statut.ETAGERE.getCount());
        System.out.println(DocBibliotheque.Statut.RESERVATION + " : " + DocBibliotheque.Statut.RESERVATION.getCount());
        System.out.println(DocBibliotheque.Statut.PILE_RETOUR + " : " + DocBibliotheque.Statut.PILE_RETOUR.getCount());
        System.out.println("Nombre de livres : " + docs.compteLivres());
        System.out.println("Nombre de CDs : " + docs.compteCDs());
        System.out.println("Appuyez sur n'importe quelle touche");

        sc.nextLine();
        mainMenu();
    }

    /* Menu docs */
    private static void menuAjouterLivre() {
        System.out.println("----------------");
        System.out.println("Entrez le code d'archivage");
        String code = sc.nextLine();

        System.out.println("Entrez le titre");
        String titre = sc.nextLine();

        System.out.println("Entrez le nom de l'auteur");
        String aut = sc.nextLine();

        System.out.println("Entrez l'année");
        int annee = getInputInt(0, Calendar.getInstance().get(Calendar.YEAR));

        System.out.println("Entrez l'isbn");
        String isbn = sc.nextLine();

        System.out.println("Entrez le nombre de pages");
        int nbPages = getInputInt(4, Integer.MAX_VALUE);

        System.out.println("Entrez le nom de l'éditeur");
        String editeur = sc.nextLine();

        docs.addLivre(code, titre, aut, annee, isbn, nbPages, editeur);

        menuDocs();
    }

    private static void menuAjouterCD() {
        System.out.println("----------------");
        System.out.println("Entrez le code d'archivage");
        String code = sc.nextLine();

        System.out.println("Entrez le titre");
        String titre = sc.nextLine();

        System.out.println("Entrez le nom de l'artiste");
        String artiste = sc.nextLine();

        System.out.println("Entrez l'année");
        int annee = sc.nextInt();
        sc.nextLine();

        docs.addCD(artiste, code, titre, annee, menuMorceaux());

        menuDocs();
    }

    private static List<String> menuMorceaux() {
        List<String> l = new ArrayList<>();
        System.out.println("Entrez les morceaux (entrer pour quitter)");
        String s = "";
        do {
            s = sc.nextLine();
            if (!s.isEmpty()) {
                l.add(s);
            }
        } while (!s.isEmpty());
        return l;
    }

    private static void menuAjouterSite() {
        System.out.println("----------------");
        System.out.println("Entrez le code d'archivage");
        String code = sc.nextLine();

        System.out.println("Entrez le titre");
        String titre = sc.nextLine();

        System.out.println("Entrez le nom de l'auteur");
        String auteur = sc.nextLine();

        System.out.println("Entrez l'année");
        int annee = getInputInt(0, Calendar.getInstance().get(Calendar.YEAR));
        sc.nextLine();

        System.out.println("Entrez l'url");
        String u = sc.nextLine();
        URL url = null;
        try {
            url = new URL(u);
        } catch (MalformedURLException e) {
            menuAjouterSite();
        }

        System.out.println("Entrez une description");
        String desc = sc.nextLine();

        docs.addDocURL(code, titre, annee, auteur, url, desc);

        menuDocs();
    }

    private static void menuListeDoc() {
        System.out.println("----------------");
        for (int i = 0; i < docs.size(); i++) {
            System.out.println(i + " - " + docs.get(i).getTitre());
        }
        System.out.println();
        System.out.println("Entrez le numéro de l'ouvrage");
        int k = getInputInt(0, Integer.MAX_VALUE);
        if (k >= 0 && k < docs.size()) {
            menuDoc(k);
        } else {
            menuDocs();
        }
    }

    private static void menuDoc(int i) {
        System.out.println("----------------");
        System.out.println(docs.get(i).toScreenString());
        System.out.println("1 - Supprimer");
        System.out.println("2 - Emprunter");
        System.out.println("3 - Utiliser par employé");
        System.out.println("4 - Reserver");
        System.out.println("5 - Annuler réservation");
        System.out.println("6 - Retourner");
        System.out.println("7 - Remettre en étagère");
        System.out.println("8 - Retour");

        int k = getInputInt(1, 8);
        switch (k) {
        case 1:
            docs.remove(i);
            break;
        case 2: {
            int l = menuMembre();
            if (l != -1) if (docs.emprunt(i, membres.get(l)))
                System.out.println("document emprunté");
            else
                System.out.println("impossible d'emprunter le document");
            menuDoc(i);
            break;
        }
        case 3: {
            int l = menuEmploye();
            if (l != -1) if (docs.reserver(i, emps.get(l)))
                System.out.println("Document utilisé par un employé");
            else
                System.out.println("Impossible d'utiliser le document");
            menuDoc(i);
        }
        case 4: {
            int l = menuMembre();
            if (l == -1)
                menuDoc(i);
            else if (docs.reserver(i, membres.get(l)))
                System.out.println("document réservé");
            else
                System.out.println("empossible de réserver le document");
            menuDoc(i);
            break;
        }
        case 5: {
            int l = menuMembre();
            if (l == -1)
                menuDoc(i);
            else if (docs.annuler(i, membres.get(l)))
                System.out.println("document annulé");
            else
                System.out.println("empossible d'annuler la réservation");
            menuDoc(i);
            break;
        }
        case 6:
            docs.retour(i);
            break;
        case 7:
            docs.remiseEtagere(i);
            break;
        case 8:
            menuDocs();
        }
    }
    
    private static int menuMembre() {
        System.out.println("----------------");
        for (int i = 0; i < membres.size(); i++) {
            System.out.println(i + " - " + membres.get(i));
        }
        
        int k = getInputInt(-1, membres.size());
        return k;
    }

    private static int menuEmploye() {
        System.out.println("----------------");
        for (int i = 0; i < emps.size(); i++) {
            System.out.println(i + " - " + emps.get(i));
        }

        int k = getInputInt(-1, emps.size());
        return k;
    }

    public static void creerMembreEtudiant(String nom, String prenom, int tel, String adresse) {
        membres.add(new MembreEtudiant(nom, prenom, tel, adresse));
    }

    public static void creerMembrePersonnel(String nom, String prenom, int tel, String adresse) {
        membres.add(new MembrePersonnel(nom, prenom, tel, adresse));
    }

    public static void creerEmployerBibliotheque(String id, String nom) {
        emps.add(new EmployeBibliotheque(id, nom));
    }

    public static int getInputInt(int min, int max) {
        int i = Integer.MIN_VALUE;
        do {
            try {
                i = sc.nextInt();
                if (i < min || i > max) throw new InputMismatchException("Out of bound");
            } catch (InputMismatchException e) {
                System.out.println("Mauvaise entrée : " + e.getMessage());
                i = Integer.MIN_VALUE;
            }
            sc.nextLine();
        } while (i < min || i > max);
        return i;
    }

    public static int getInputInt() {
        return getInputInt(Integer.MIN_VALUE, Integer.MAX_VALUE);
    }
}
