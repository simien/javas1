package fr.scarex.iut.tp1;

/**
 * @author SCAREX
 * 
 */
public class Impot
{

    public static void main(String[] args) {
        System.out.println("Impot 57000 : " + impot(57000));
    }

    public static double impot(double revenu) {
        if (revenu <= 0) return 0;
        double impot = 0;
        if (revenu > 20000) {
            impot = 1000;
            if (revenu > 40000) {
                impot += 2000;
                if (revenu > 60000) {
                    impot += revenu * 0.3;
                } else {
                    impot += (revenu - 40000) * 0.15;
                }
            } else {
                impot += (revenu - 20000) * .1;
            }
        } else {
            impot = revenu * .05;
        }
        return impot;
    }
}
