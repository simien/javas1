package fr.scarex.iut.tp1;

/**
 * @author SCAREX
 * 
 */
public class Conversion
{
    public static void main(String[] args) {
        conversion('D');
        conversion('b');
        conversion(']');
    }
    
    public static void conversion(char c) {
        if (c >= 65 && c <= 90) {
            c += 32;
            System.out.println("Le charactère est une majuscule : " + c);
        } else if (c >= 97 && c <= 122) {
            c -= 32;
            System.out.println("Le charactère est une minuscule : " + c);
        } else {
            System.out.println("Le charactère n'est pas une lettre : " + c);
        }
    }
}
