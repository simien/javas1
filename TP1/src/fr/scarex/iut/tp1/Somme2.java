package fr.scarex.iut.tp1;


/**
 * @author SCAREX
 * 
 */
public class Somme2
{
    public static void main(String[] args) {
        System.out.println(calculSomme());
    }

    public static int calculSomme() {
        int somme = 0;
        for (int i = 1; i <= 100; i++)
            somme = somme + i;
        return somme;
    }
}
