package fr.scarex.biblio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author SCAREX
 *
 */
public class CD extends DocBibliotheque
{
    protected String artiste;
    protected List<String> morceaux = new ArrayList<>();
    
    public CD(String artiste, String codeArchivage, String titre, int annee, String ... morceaux) {
        this(artiste, codeArchivage, titre, annee, Arrays.asList(morceaux));
    }
    
    public CD(String artiste, String codeArchivage, String titre, int annee, List<String> morceaux) {
        super(codeArchivage, titre, annee);
        this.artiste = artiste;
        this.morceaux = morceaux;
    }

    /**
     * @return the artiste
     */
    public String getArtiste() {
        return artiste;
    }

    /**
     * @param artiste the artiste to set
     */
    public void setArtiste(String artiste) {
        this.artiste = artiste;
    }

    @Override
    public String getCreateur() {
        return this.getArtiste();
    }

    @Override
    public void setCreateur(String createur) {
        this.setArtiste(createur);
    }

    @Override
    public boolean empruntable() {
        return true;
    }

    @Override
    public String toScreenString() {
        return String.format("CD : artiste=%s, morceaux=%s", artiste, morceaux);
    }
}
