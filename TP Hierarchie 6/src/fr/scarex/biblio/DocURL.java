package fr.scarex.biblio;

import java.net.URL;

/**
 * @author SCAREX
 *
 */
public class DocURL extends DocBibliotheque
{
    protected String auteur;
    protected URL url;
    protected String desc;

    public DocURL(String codeArchivage, String titre, int annee, String auteur, URL url, String desc) {
        super(codeArchivage, titre, annee);
        this.auteur = auteur;
        this.url = url;
        this.desc = desc;
    }

    /**
     * @return the auteur
     */
    public String getAuteur() {
        return auteur;
    }

    /**
     * @param auteur the auteur to set
     */
    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    /**
     * @return the url
     */
    public URL getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(URL url) {
        this.url = url;
    }

    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc the desc to set
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String getCreateur() {
        return this.getAuteur();
    }

    @Override
    public void setCreateur(String createur) {
        this.setAuteur(createur);
    }

    @Override
    public boolean empruntable() {
        return false;
    }

    @Override
    public String toString() {
        return String.format("DocURL : auteur=%s, url=%s, desc=%s", auteur, url, desc);
    }
}
