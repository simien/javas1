package fr.scarex.biblio;

/**
 * @author SCAREX
 *
 */
public class MembrePersonnel extends MembreBibliotheque
{
    public MembrePersonnel(String nom, String prenom, int tel, String adresse) {
        super(nom, prenom, tel, adresse);
    }

    @Override
    public boolean peutEmprunterAutreDocument() {
        return this.nbEmprunts < 8;
    }
}
