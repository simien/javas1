package personnel.vue;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.TreeSet;

import javax.sql.DataSource;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import personnel.dao.IAdministratifDAO;
import personnel.dao.oracle.OracleAdministratifDAO;
import personnel.dao.oracle.OracleDataSourceDAO;
import personnel.metier.Administratif;
import personnel.metier.Personnel;

/**
 * @author SCAREX
 * 
 */
@SuppressWarnings("serial")
public class Fenetre extends JFrame
{
    public JLabel stateLabel;
    public JTextField tfNumAdmin;
    public JTextField tfNomAdmin;
    public JTextField tfNumBureau;
    public JTextField tfSalMens;
    public JButton bprev;
    public JButton bsuiv;
    public JButton bajout;
    public JButton bsuprr;
    private TreeSet<Personnel> listeClasseAdmins = new TreeSet<>();
    private static IAdministratifDAO adminDAO;
    private static DataSource dataSourceDAO;
    private static Connection connectionBD;
    private Personnel selectedItem = null;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Fenetre();
            }
        });
    }

    public Fenetre() {
        setTitle("Oui");
        setSize(400, 400);
        setResizable(true);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setContentPane(buildPanel());

        setVisible(true);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    if (connectionBD != null) connectionBD.close();
                } catch (SQLException se) {
                    se.printStackTrace();
                }
            }
        });

        (new Thread() {
            @Override
            public void run() {
                try {
                    stateLabel.setText("Init Connection");
                    dataSourceDAO = OracleDataSourceDAO.getOracleDataSourceDAO();
                    adminDAO = new OracleAdministratifDAO();
                    adminDAO.setDataSource(dataSourceDAO);
                    connectionBD = dataSourceDAO.getConnection();
                    adminDAO.setConnection(connectionBD);
                    stateLabel.setText("Init terminated");
                    updateAdmins(true);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void updateAdmins(boolean reload) {
        if (reload) {
            stateLabel.setText("Loading admins...");
            Collections.addAll(listeClasseAdmins, adminDAO.getAdmins().toArray(new Personnel[0]));
            stateLabel.setText("Admins loaded");
        }
        if (selectedItem == null || !listeClasseAdmins.contains(selectedItem)) {
            selectedItem = listeClasseAdmins.first();
        }
        tfNumAdmin.setText("" + listeClasseAdmins.ceiling(selectedItem).getNumeroPersonnel());
        tfNomAdmin.setText("" + listeClasseAdmins.ceiling(selectedItem).getNomPersonnel());
        tfNumBureau.setText("" + listeClasseAdmins.ceiling(selectedItem).getNumeroBureau());
        tfSalMens.setText("" + listeClasseAdmins.ceiling(selectedItem).calculerSalaire());
    }

    private JPanel buildPanel() {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());

        JPanel topP = new JPanel();
        topP.add(new JLabel("Liste des administratifs"));
        topP.add(stateLabel = new JLabel());
        mainPanel.add(topP, BorderLayout.NORTH);

        mainPanel.add(buildMiddlePanel(mainPanel), BorderLayout.CENTER);
        mainPanel.add(buildSouthPanel(mainPanel), BorderLayout.SOUTH);

        return mainPanel;
    }

    private Component buildMiddlePanel(JPanel j) {
        JPanel p = new JPanel();
        p.setLayout(new GridLayout(4, 2));

        p.add(new JLabel("Numéro Administratif :"));
        p.add(tfNumAdmin = new JTextField(3));

        p.add(new JLabel("Nom administratif :"));
        p.add(tfNomAdmin = new JTextField(60));

        p.add(new JLabel("Numéro de Bureau :"));
        p.add(tfNumBureau = new JTextField(3));

        p.add(new JLabel("Salaire mensuel"));
        p.add(tfSalMens = new JTextField(8));

        return p;
    }

    private Component buildSouthPanel(JPanel mainPanel) {
        JPanel p = new JPanel();
        p.setLayout(new GridLayout(2, 2));

        p.add(bprev = new JButton("<<"));
        bprev.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectedItem = listeClasseAdmins.first() == selectedItem ? listeClasseAdmins.last() : listeClasseAdmins.lower(selectedItem);
                updateAdmins(false);
            }
        });
        p.add(bsuiv = new JButton(">>"));
        bsuiv.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectedItem = listeClasseAdmins.higher(selectedItem);
                updateAdmins(false);
            }
        });
        p.add(bajout = new JButton("Ajouter"));
        bajout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Administratif a = new Administratif(Integer.parseInt(tfNumAdmin.getText()), tfNomAdmin.getText(), Integer.parseInt(tfNumBureau.getText()), Double.parseDouble(tfSalMens.getText()));
                adminDAO.creerAdmin(a);
                updateAdmins(true);
                for (Personnel p : listeClasseAdmins) {
                    if (p.getNumeroPersonnel() == a.getNumeroPersonnel()) {
                        selectedItem = p;
                        break;
                    }
                }
                updateAdmins(false);
            }
        });
        p.add(bsuprr = new JButton("Supprimer"));
        bsuprr.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                adminDAO.supprimerAdmin(selectedItem.getNumeroPersonnel());
                selectedItem = null;
                updateAdmins(true);
            }
        });

        return p;
    }
}
