package personnel.dao;

import java.sql.Connection;
import java.util.List;

import javax.sql.DataSource;

import personnel.metier.Administratif;
import personnel.metier.Personnel;

/**
 * @author SCAREX
 *
 */
public interface IAdministratifDAO
{
    public void setConnection(Connection c);
    
    public void setDataSource(DataSource ds);
    
    public List<Personnel> getAdmins();
    
    public void creerAdmin(Administratif a);
    
    public void supprimerAdmin(int numAdmin);
}
