package personnel.dao.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import personnel.dao.IAdministratifDAO;
import personnel.metier.Administratif;
import personnel.metier.Personnel;

/**
 * @author SCAREX
 * 
 */
public class OracleAdministratifDAO implements IAdministratifDAO
{
    public DataSource ds;
    public Connection connectionBD;

    @Override
    public void setConnection(Connection c) {
        connectionBD = c;
    }

    @Override
    public void setDataSource(DataSource ds) {
        this.ds = ds;
    }

    @Override
    public List<Personnel> getAdmins() {
        Statement stmt = null;
        ResultSet rs = null;
        List<Personnel> l = null;
        try {
            stmt = connectionBD.createStatement();
            l = new ArrayList<>();
            rs = stmt.executeQuery("SELECT * FROM ADMINISTRATIF");
            while (rs.next()) {
                Administratif a = new Administratif(rs.getString("NOMADMINISTRATIF"), rs.getInt("NUMADMINISTRATIF"), rs.getDouble("SALAIREMENSUEL"));
                a.setNumeroBureau(rs.getInt("NUMEROBUREAU"));
                l.add(a);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return l;
    }

    @Override
    public void creerAdmin(Administratif a) {
        PreparedStatement stmt = null;
        try {
            stmt = connectionBD.prepareStatement("INSERT INTO ADMINISTRATIF values (?,?,?,?)");
            stmt.setInt(1, a.getNumeroPersonnel());
            stmt.setString(2, a.getNomPersonnel());
            stmt.setInt(3, a.getNumeroBureau());
            stmt.setDouble(4, a.getSal());
            
            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void supprimerAdmin(int numAdmin) {
        Statement stmt = null;
        try {
            stmt = connectionBD.createStatement();
            stmt.execute("DELETE FROM ADMINISTRATIF WHERE NUMADMINISTRATIF = " + numAdmin);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
