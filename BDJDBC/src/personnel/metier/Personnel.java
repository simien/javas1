package personnel.metier;

/**
 * @author SCAREX
 *
 */
public abstract class Personnel
{
    private int numeroPersonnel;
    private String nomPersonnel;
    private int numeroBureau;
    public static int dernierNumeroPersonnel = 1;
    
    public Personnel(int numeroPersonnel, String nomPersonnel, int numeroBureau) {
        this.numeroPersonnel = numeroPersonnel;
        this.nomPersonnel = nomPersonnel;
        this.numeroBureau = numeroBureau;
    }
    public Personnel(String nom, int num) {
        this.numeroPersonnel = num;
        this.nomPersonnel = nom;
    }
    /**
     * @return the numeroPersonnel
     */
    public int getNumeroPersonnel() {
        return numeroPersonnel;
    }

    /**
     * @param numeroPersonnel the numeroPersonnel to set
     */
    public void setNumeroPersonnel(int numeroPersonnel) {
        this.numeroPersonnel = numeroPersonnel;
    }

    /**
     * @return the nomPersonnel
     */
    public String getNomPersonnel() {
        return nomPersonnel;
    }

    /**
     * @param nomPersonnel the nomPersonnel to set
     */
    public void setNomPersonnel(String nomPersonnel) {
        this.nomPersonnel = nomPersonnel;
    }

    /**
     * @return the numeroBureau
     */
    public int getNumeroBureau() {
        return numeroBureau;
    }
    
    /**
     * @param numeroBureau the numeroBureau to set
     */
    public void setNumeroBureau(int numeroBureau) {
        this.numeroBureau = numeroBureau;
    }

    public abstract double calculerSalaire();

    @Override
    public String toString() {
        return String.format("Personnel [numeroPersonnel=%s, nomPersonnel=%s, numeroBureau=%s]", numeroPersonnel, nomPersonnel, numeroBureau);
    }
}
