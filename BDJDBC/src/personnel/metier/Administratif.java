package personnel.metier;

/**
 * @author SCAREX
 *
 */
public class Administratif extends Personnel implements Comparable<Administratif>
{
    private double sal;
    
    public Administratif(int numeroPersonnel, String nomPersonnel, int numeroBureau, double sal) {
        super(numeroPersonnel, nomPersonnel, numeroBureau);
        this.sal = sal;
    }

    public Administratif(String nom, int num, double sal) {
        super(nom, num);
        this.sal = sal;
    }

    @Override
    public double calculerSalaire() {
        return sal;
    }

    /**
     * @return the sal
     */
    public double getSal() {
        return sal;
    }

    /**
     * @param sal the sal to set
     */
    public void setSal(double sal) {
        this.sal = sal;
    }

    @Override
    public String toString() {
        return String.format("Administratif [sal=%s]", sal);
    }

    @Override
    public int compareTo(Administratif o) {
        return Integer.compare(this.getNumeroPersonnel(), o.getNumeroPersonnel());
    }
}
