package fr.simien.tp3;

/**
 * @author SCAREX
 *
 */
public class TP3Test
{
    public static void main(String[] args) {
        Proprietaire prop = new Proprietaire("Damien", "Blettery", "8 rue du Tajine, Villeurbanne 69100 France", 600, 200);
        Locataire loc = new Locataire("Simon", "Chaumet", "3 rue antoine de saint exupery, Meximieux 01800 France", 300, 150);
        BienImmobilier im = new BienImmobilier(10000D, true, false, prop, loc);
        System.out.println(im);
    }
}
