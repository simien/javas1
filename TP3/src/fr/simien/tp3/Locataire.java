package fr.simien.tp3;

/**
 * @author SCAREX
 *
 */
public class Locataire
{
    private String prenom;
    private String nom;
    private String adresse;
    private int loyer;
    private int taxeHabitation;
    
    public Locataire(String prenom, String nom, String adresse, int loyer, int taxeHabitation) {
        this.prenom = prenom;
        this.nom = nom;
        this.adresse = adresse;
        this.loyer = loyer;
        this.taxeHabitation = taxeHabitation;
    }

    public String toString() {
        return "Locataire [prenom=" + prenom + ", nom=" + nom + ", adresse=" + adresse + ", loyer=" + loyer + ", taxeHabitation=" + taxeHabitation + "]";
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the adresse
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * @param adresse the adresse to set
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * @return the loyer
     */
    public int getLoyer() {
        return loyer;
    }

    /**
     * @param loyer the loyer to set
     */
    public void setLoyer(int loyer) {
        this.loyer = loyer;
    }

    /**
     * @return the taxeHabitation
     */
    public int getTaxeHabitation() {
        return taxeHabitation;
    }

    /**
     * @param taxeHabitation the taxeHabitation to set
     */
    public void setTaxeHabitation(int taxeHabitation) {
        this.taxeHabitation = taxeHabitation;
    }
}
