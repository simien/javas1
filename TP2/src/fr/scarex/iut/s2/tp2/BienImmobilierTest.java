package fr.scarex.iut.s2.tp2;

/**
 * @author SCAREX
 *
 */
public class BienImmobilierTest
{
    public static void main(String[] args) {
        BienImmobilier b1 = new BienImmobilier(2000, false, true, "Jean Jacques", null);
        BienImmobilier b2 = new BienImmobilier(200, true, false, "Damien", "Sims 38");
        System.out.println(b1);
        System.out.println(b2);
    }
}
