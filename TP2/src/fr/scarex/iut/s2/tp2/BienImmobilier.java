package fr.scarex.iut.s2.tp2;

/**
 * @author SCAREX
 * 
 */
public class BienImmobilier
{
    private static int refindex = 0;
    private final int ref;
    private double price;
    private boolean loc;
    private boolean sell;
    private String propname;
    private String locname;

    public BienImmobilier(double price, boolean loc, boolean sell, String propname, String locname) {
        this.ref = BienImmobilier.getNextIndex();
        this.price = price;
        this.loc = loc;
        this.sell = sell;
        this.propname = propname;
        this.locname = locname;
    }

    private static int getNextIndex() {
        return BienImmobilier.refindex++;
    }

    /**
     * @return the loc
     */
    public boolean estEnLocation() {
        return loc;
    }

    /**
     * @return the sell
     */
    public boolean estEnVente() {
        return sell;
    }

    /**
     * @param locname
     *            the locname to set
     */
    public void location(String locname) {
        this.locname = locname;
    }

    public void finLocation() {
        this.loc = false;
        this.locname = null;
    }

    public void vente(double prixPropose, String nomProprietaire) {
        this.price = prixPropose;
        this.propname = nomProprietaire;
    }

    /**
     * @return the refindex
     */
    public static int getRefindex() {
        return refindex;
    }

    /**
     * @param refindex
     *            the refindex to set
     */
    public static void setRefindex(int refindex) {
        BienImmobilier.refindex = refindex;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the loc
     */
    public boolean isLoc() {
        return loc;
    }

    /**
     * @param loc
     *            the loc to set
     */
    public void setLoc(boolean loc) {
        this.loc = loc;
    }

    /**
     * @return the sell
     */
    public boolean isSell() {
        return sell;
    }

    /**
     * @param sell
     *            the sell to set
     */
    public void setSell(boolean sell) {
        this.sell = sell;
    }

    /**
     * @return the propname
     */
    public String getPropname() {
        return propname;
    }

    /**
     * @param propname
     *            the propname to set
     */
    public void setPropname(String propname) {
        this.propname = propname;
    }

    /**
     * @return the locname
     */
    public String getLocname() {
        return locname;
    }

    /**
     * @param locname
     *            the locname to set
     */
    public void setLocname(String locname) {
        this.locname = locname;
    }

    /**
     * @return the ref
     */
    public int getRef() {
        return ref;
    }

    public String toString() {
        return "Bien immobilier de ref : " + ref + ", de prix : " + price + ", location : " + loc + ", en vente : " + sell + ", propriétaire : " + propname + ", locataire : " + locname;
    }
}
