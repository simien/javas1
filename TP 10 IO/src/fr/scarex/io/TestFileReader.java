package fr.scarex.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author SCAREX
 * 
 */
public class TestFileReader
{
    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Entrez l'emplacement du fichier :");
        try {
            String emp = br.readLine();

            FileReader fr = new FileReader(new File(System.getenv("user.dir"), emp));

            BufferedReader bfr = new BufferedReader(fr);

            List<Character> l = new ArrayList<>();
            int k;
            while ((k = bfr.read()) != -1) {
                l.add((char) k);
            }
            fr.close();

            for (char c : l) {
                System.out.print(c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
