package fr.scarex.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author SCAREX
 * 
 */
public class TestFileWriter
{
    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Entrez l'emplacement du fichier :");
        try {
            String emp = br.readLine();
            
            FileWriter fw = new FileWriter(new File(System.getenv("user.dir"), emp));
            String s = "";
            
            int i = 1;
            
            do {
                s = br.readLine();
                if (!s.equals("fin")) {
                    fw.write(i + " - " + s + "\n");
                    fw.flush();
                    i++;
                }
            } while (!s.equals("fin"));
            
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
