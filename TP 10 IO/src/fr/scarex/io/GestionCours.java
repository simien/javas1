package fr.scarex.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

/**
 * @author SCAREX
 * 
 */
public class GestionCours
{
    private static File fread = new File(System.getenv("user.dir"), "testreadobj.txt");
    private static File fwrite = new File(System.getenv("user.dir"), "testwriteobj.txt");
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Voulez vous lire ou écrire un cours ?");
        System.out.println("1 - Lire");
        System.out.println("2 - Écrire");
        
        int ile = sc.nextInt();
        sc.nextLine();
        
        if (ile == 1) {
            ObjectInputStream ois = null;
            try {
                ois = new ObjectInputStream(new FileInputStream(fread));
                Cours c = (Cours) ois.readObject();
                System.out.println("Cours trouvé : ");
                System.out.println(c);
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (ile == 2) {
            System.out.println("Entrez le code module");
            String codeModule = sc.nextLine();
            
            System.out.println("Entrez le numero du cours");
            String numCours = sc.nextLine();
            
            Cours c = new Cours(codeModule, numCours);
            ObjectOutputStream oos = null;
            try {
                oos = new ObjectOutputStream(new FileOutputStream(fwrite));
                oos.writeObject(c);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
