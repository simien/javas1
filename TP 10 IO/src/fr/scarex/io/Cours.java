package fr.scarex.io;

import java.io.Serializable;

public class Cours implements Serializable
{
    private String codeModule;
    private String numCours;

    public Cours() {
        codeModule = "NonSpecifie";
        numCours = "0";
    }

    public Cours(String codeModule, String numCours) {
        this.codeModule = codeModule;
        this.numCours = numCours;
    }

    public String getNumCours() {
        return this.numCours;
    }

    public String getCodeModule() {
        return this.codeModule;
    }

    public void setNumCours(String numCours) {
        this.numCours = numCours;
    }

    public void setCodeModule(String codeModule) {
        this.codeModule = codeModule;
    }

    public String toString() {
        return "Code du module : " + codeModule + " numéro du cours : " + numCours;
    }

}