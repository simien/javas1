package fr.scarex.minibus.dao;

import java.sql.Connection;
import java.util.List;

import javax.sql.DataSource;

import fr.scarex.minibus.metier.Minibus;

/**
 * @author SCAREX
 *
 */
public interface IMinibusDAO
{
    public void setConnection(Connection c);

    public void setDataSource(DataSource ds);

    public List<Minibus> getMinibuses();

    public void creerMinibus(Minibus a);

    public void supprimerMinibus(int numMinibus);
}
