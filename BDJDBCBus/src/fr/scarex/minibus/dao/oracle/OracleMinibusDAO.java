package fr.scarex.minibus.dao.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import fr.scarex.minibus.dao.IMinibusDAO;
import fr.scarex.minibus.metier.Minibus;

/**
 * @author SCAREX
 * 
 */
public class OracleMinibusDAO implements IMinibusDAO
{
    public DataSource ds;
    public Connection connectionBD;

    @Override
    public void setConnection(Connection c) {
        connectionBD = c;
    }

    @Override
    public void setDataSource(DataSource ds) {
        this.ds = ds;
    }

    @Override
    public List<Minibus> getMinibuses() {
        Statement stmt = null;
        ResultSet rs = null;
        List<Minibus> l = null;
        try {
            stmt = connectionBD.createStatement();
            l = new ArrayList<>();
            rs = stmt.executeQuery("SELECT * FROM Minibus");
            while (rs.next()) {
                l.add(new Minibus(rs.getInt("NOMINIBUS"), rs.getInt("CAPACITE")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return l;
    }

    @Override
    public void creerMinibus(Minibus m) {
        PreparedStatement stmt = null;
        try {
            stmt = connectionBD.prepareStatement("INSERT INTO Minibus values (?,?)");
            stmt.setInt(1, m.getNumMinibus());
            stmt.setInt(2, m.getCapacite());

            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void supprimerMinibus(int numMinibus) {
        Statement stmt = null;
        try {
            stmt = connectionBD.createStatement();
            stmt.execute("DELETE FROM Minibus WHERE NOMINIBUS = " + numMinibus);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
