package fr.scarex.minibus.dao.oracle;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

import oracle.jdbc.pool.OracleDataSource;

/**
 * @author SCAREX
 * 
 */
@SuppressWarnings("serial")
public class OracleDataSourceDAO extends OracleDataSource
{
    private static OracleDataSourceDAO ods;

    private OracleDataSourceDAO() throws SQLException {}

    public static OracleDataSourceDAO getOracleDataSourceDAO() {
        FileInputStream fichier = null;
        try {
            Properties props = new Properties();
            fichier = new FileInputStream(new File(System.getenv("user.dir"), "oracledbprop.txt"));
            props.load(fichier);
            ods = new OracleDataSourceDAO();
            ods.setDriverType(props.getProperty("pilote"));
            ods.setPortNumber(new Integer(props.getProperty("port")));
            ods.setServiceName(props.getProperty("service"));
            ods.setUser(props.getProperty("user"));
            ods.setPassword(props.getProperty("pwd"));
            ods.setServerName(props.getProperty("serveur"));
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fichier.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ods;
    }
}
