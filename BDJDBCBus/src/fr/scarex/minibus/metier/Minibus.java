package fr.scarex.minibus.metier;

/**
 * @author SCAREX
 *
 */
public class Minibus implements Comparable<Minibus>
{
    private int numMinibus;
    private int capacite;

    public Minibus(int numMinibus, int capacite) {
        this.numMinibus = numMinibus;
        this.capacite = capacite;
    }

    /**
     * @return the numMinibus
     */
    public int getNumMinibus() {
        return numMinibus;
    }

    /**
     * @param numMinibus
     *            the numMinibus to set
     */
    public void setNumMinibus(int numMinibus) {
        this.numMinibus = numMinibus;
    }

    /**
     * @return the capacite
     */
    public int getCapacite() {
        return capacite;
    }

    /**
     * @param capacite
     *            the capacite to set
     */
    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    @Override
    public int compareTo(Minibus m) {
        return Integer.compare(this.numMinibus, m.numMinibus);
    }
}
