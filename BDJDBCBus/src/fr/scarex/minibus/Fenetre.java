package fr.scarex.minibus;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.TreeSet;

import javax.sql.DataSource;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.scarex.minibus.dao.IMinibusDAO;
import fr.scarex.minibus.dao.oracle.OracleDataSourceDAO;
import fr.scarex.minibus.dao.oracle.OracleMinibusDAO;
import fr.scarex.minibus.metier.Minibus;

/**
 * @author SCAREX
 *
 */
@SuppressWarnings("serial")
public class Fenetre extends JFrame
{
    public JLabel stateLabel;
    public JTextField tfNumMinibus;
    public JTextField tfCapacite;
    public JButton bprev;
    public JButton bsuiv;
    private TreeSet<Minibus> listeMinibus = new TreeSet<>();
    private static IMinibusDAO minibusDAO;
    private static DataSource dataSourceDAO;
    private static Connection connectionBD;
    private Minibus selectedItem = null;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Fenetre();
            }
        });
    }

    public Fenetre() {
        setTitle("Minibus");
        setSize(400, 400);
        setResizable(true);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setContentPane(buildPanel());

        setVisible(true);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    if (connectionBD != null) connectionBD.close();
                } catch (SQLException se) {
                    se.printStackTrace();
                }
            }
        });

        (new Thread() {
            @Override
            public void run() {
                try {
                    stateLabel.setText("Init Connection");
                    dataSourceDAO = OracleDataSourceDAO.getOracleDataSourceDAO();
                    minibusDAO = new OracleMinibusDAO();
                    minibusDAO.setDataSource(dataSourceDAO);
                    connectionBD = dataSourceDAO.getConnection();
                    minibusDAO.setConnection(connectionBD);
                    stateLabel.setText("Init terminated");
                    updateMinibus(true);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void updateMinibus(boolean reload) {
        if (reload) {
            stateLabel.setText("Loading minibus...");
            Collections.addAll(listeMinibus, minibusDAO.getMinibuses().toArray(new Minibus[0]));
            stateLabel.setText("Minibus loaded");
        }
        if (selectedItem == null || !listeMinibus.contains(selectedItem)) {
            selectedItem = listeMinibus.first();
        }
        tfNumMinibus.setText("" + listeMinibus.ceiling(selectedItem).getNumMinibus());
        tfCapacite.setText("" + listeMinibus.ceiling(selectedItem).getCapacite());
    }

    private JPanel buildPanel() {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());

        JPanel topP = new JPanel();
        topP.add(new JLabel("Liste des minibus"));
        topP.add(stateLabel = new JLabel());
        mainPanel.add(topP, BorderLayout.NORTH);

        mainPanel.add(buildMiddlePanel(mainPanel), BorderLayout.CENTER);
        mainPanel.add(buildSouthPanel(mainPanel), BorderLayout.SOUTH);

        return mainPanel;
    }

    private Component buildMiddlePanel(JPanel j) {
        JPanel p = new JPanel();
        p.setLayout(new GridLayout(2, 2));

        JPanel p1 = new JPanel(new GridBagLayout());
        JPanel p2 = new JPanel(new GridBagLayout());
        JPanel p3 = new JPanel(new GridBagLayout());
        JPanel p4 = new JPanel(new GridBagLayout());

        p1.add(new JLabel("Numéro Minibus :"));
        p2.add(tfNumMinibus = new JTextField(3));
        tfNumMinibus.setColumns(6);

        p3.add(new JLabel("Capacité :"));
        p4.add(tfCapacite = new JTextField(3));
        tfCapacite.setColumns(6);

        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);

        return p;
    }

    private Component buildSouthPanel(JPanel mainPanel) {
        JPanel p = new JPanel();
        p.setLayout(new GridLayout(1, 2));

        JPanel p1 = new JPanel(new GridBagLayout());
        JPanel p2 = new JPanel(new GridBagLayout());

        p1.add(bprev = new JButton("<<"));
        bprev.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectedItem = listeMinibus.first() == selectedItem ? listeMinibus.last() : listeMinibus.lower(selectedItem);
                updateMinibus(false);
            }
        });
        p2.add(bsuiv = new JButton(">>"));
        bsuiv.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectedItem = listeMinibus.higher(selectedItem);
                updateMinibus(false);
            }
        });

        p.add(p1);
        p.add(p2);

        return p;
    }
}
