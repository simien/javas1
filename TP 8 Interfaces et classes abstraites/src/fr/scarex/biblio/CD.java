package fr.scarex.biblio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.scarex.biblio.DocBibliotheque.Statut;
import fr.scarex.biblio.exception.AnnulReservException;
import fr.scarex.biblio.exception.EmpruntException;
import fr.scarex.biblio.exception.ReserveException;
import fr.scarex.biblio.exception.RetourException;
import fr.scarex.biblio.interfaces.IEmpruntable;
import fr.scarex.biblio.interfaces.INotifiable;

/**
 * @author SCAREX
 *
 */
public class CD extends DocBibliotheque implements IEmpruntable
{
    protected String artiste;
    protected List<String> morceaux = new ArrayList<>();
    
    public CD(String artiste, String codeArchivage, String titre, int annee, String ... morceaux) {
        this(artiste, codeArchivage, titre, annee, Arrays.asList(morceaux));
    }
    
    public CD(String artiste, String codeArchivage, String titre, int annee, List<String> morceaux) {
        super(codeArchivage, titre, annee);
        this.artiste = artiste;
        this.morceaux = morceaux;
    }

    /**
     * @return the artiste
     */
    public String getArtiste() {
        return artiste;
    }

    /**
     * @param artiste the artiste to set
     */
    public void setArtiste(String artiste) {
        this.artiste = artiste;
    }

    @Override
    public String getCreateur() {
        return this.getArtiste();
    }

    @Override
    public void setCreateur(String createur) {
        this.setArtiste(createur);
    }
    
    public boolean emprunt(MembreBibliotheque m) throws EmpruntException {
        if (this.statut == Statut.ETAGERE) {
            this.setStatut(Statut.EMPRUNTE);
            this.emprunteur = m;
            return true;
        } else if (this.statut == Statut.RESERVATION && m == this.reserveur) {
            this.emprunteur = m;
            this.reserveur = null;
            this.setStatut(Statut.EMPRUNTE);
            return true;
        }
        throw new EmpruntException();
    }

    public boolean reserver(INotifiable m) throws ReserveException {
        if (this.reserveur == null && this.statut == Statut.EMPRUNTE) {
            this.reserveur = m;
            return true;
        }
        throw new ReserveException();
    }

    public boolean retour() throws RetourException {
        if (this.statut == Statut.EMPRUNTE && this.reserveur != null) {
            this.setStatut(Statut.RESERVATION);
            this.emprunteur = null;
            this.reserveur.docDisponible(this);
            return true;
        } else if (this.statut == Statut.EMPRUNTE) {
            this.setStatut(Statut.PILE_RETOUR);
            this.emprunteur = null;
            return true;
        }
        throw new RetourException();
    }

    public boolean annulerReservation(MembreBibliotheque m) throws AnnulReservException {
        if (this.reserveur == m) {
            if (this.statut == Statut.RESERVATION) {
                this.setStatut(Statut.PILE_RETOUR);
                this.reserveur = null;
                return true;
            } else if (this.statut == Statut.EMPRUNTE) {
                this.reserveur = null;
                return true;
            }
        }
        throw new AnnulReservException();
    }

    @Override
    public String toScreenString() {
        return String.format("CD : artiste=%s, morceaux=%s", artiste, morceaux);
    }
}
