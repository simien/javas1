package fr.scarex.biblio;

/**
 * @author SCAREX
 *
 */
public class MembrePersonnel extends MembreBibliotheque
{
    public MembrePersonnel(String nom, String prenom, int tel, String adresse) {
        super(nom, prenom, tel, adresse);
    }

    @Override
    public boolean peutEmprunterAutreDocument() {
        return this.nbEmprunts < 8;
    }

    @Override
    public void docDisponible(DocBibliotheque d) {
        System.out.println("Le document " + d.titre + " qui a été réservé par le membre du personnel " + this.nom + " " + this.prenom + " est désormais disponible à l’emprunt au bureau des réservations");
    }
}
