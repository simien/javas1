package fr.scarex.biblio.interfaces;

import fr.scarex.biblio.MembreBibliotheque;
import fr.scarex.biblio.exception.AnnulReservException;
import fr.scarex.biblio.exception.EmpruntException;
import fr.scarex.biblio.exception.ReserveException;
import fr.scarex.biblio.exception.RetourException;

/**
 * @author SCAREX
 *
 */
public interface IEmpruntable
{
    public boolean emprunt(MembreBibliotheque m) throws EmpruntException;
    
    public boolean retour() throws RetourException;
    
    public boolean reserver(INotifiable m) throws ReserveException;
    
    public boolean annulerReservation(MembreBibliotheque m) throws AnnulReservException;
}
