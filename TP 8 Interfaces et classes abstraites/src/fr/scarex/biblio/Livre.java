package fr.scarex.biblio;

import fr.scarex.biblio.DocBibliotheque.Statut;
import fr.scarex.biblio.exception.AnnulReservException;
import fr.scarex.biblio.exception.EmpruntException;
import fr.scarex.biblio.exception.ReserveException;
import fr.scarex.biblio.exception.RetourException;
import fr.scarex.biblio.interfaces.IEmpruntable;
import fr.scarex.biblio.interfaces.INotifiable;

/**
 * @author SCAREX
 *
 */
public class Livre extends DocBibliotheque implements IEmpruntable
{
    private String isbn;
    private int nbPages;
    private String editeur;
    protected String auteur;
    
    public Livre(String codeArchivage, String titre, String auteur, int annee, String isbn, int nbPages, String editeur) {
        super(codeArchivage, titre, annee);
        this.isbn = isbn;
        this.nbPages = nbPages;
        this.editeur = editeur;
    }

    /**
     * @return the isbn
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * @param isbn the isbn to set
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    /**
     * @return the nbPages
     */
    public int getNbPages() {
        return nbPages;
    }

    /**
     * @param nbPages the nbPages to set
     */
    public void setNbPages(int nbPages) {
        this.nbPages = nbPages;
    }

    /**
     * @return the editeur
     */
    public String getEditeur() {
        return editeur;
    }

    /**
     * @param editeur the editeur to set
     */
    public void setEditeur(String editeur) {
        this.editeur = editeur;
    }

    /**
     * @return the auteur
     */
    public String getAuteur() {
        return auteur;
    }

    /**
     * @param auteur the auteur to set
     */
    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    @Override
    public String getCreateur() {
        return this.getAuteur();
    }

    @Override
    public void setCreateur(String createur) {
        this.setAuteur(createur);
    }
    
    public boolean emprunt(MembreBibliotheque m) throws EmpruntException {
        if (this.statut == Statut.ETAGERE) {
            this.setStatut(Statut.EMPRUNTE);
            this.emprunteur = m;
            return true;
        } else if (this.statut == Statut.RESERVATION && m == this.reserveur) {
            this.emprunteur = m;
            this.reserveur = null;
            this.setStatut(Statut.EMPRUNTE);
            return true;
        }
        throw new EmpruntException();
    }

    public boolean reserver(INotifiable m) throws ReserveException {
        if (this.reserveur == null && this.statut == Statut.EMPRUNTE) {
            this.reserveur = m;
            return true;
        }
        throw new ReserveException();
    }

    public boolean retour() throws RetourException {
        if (this.statut == Statut.EMPRUNTE && this.reserveur != null) {
            this.setStatut(Statut.RESERVATION);
            this.emprunteur = null;
            this.reserveur.docDisponible(this);
            return true;
        } else if (this.statut == Statut.EMPRUNTE) {
            this.setStatut(Statut.PILE_RETOUR);
            this.emprunteur = null;
            return true;
        }
        throw new RetourException();
    }

    public boolean annulerReservation(MembreBibliotheque m) throws AnnulReservException {
        if (this.reserveur == m) {
            if (this.statut == Statut.RESERVATION) {
                this.setStatut(Statut.PILE_RETOUR);
                this.reserveur = null;
                return true;
            } else if (this.statut == Statut.EMPRUNTE) {
                this.reserveur = null;
                return true;
            }
        }
        throw new AnnulReservException();
    }

    @Override
    public String toScreenString() {
        return String.format("Livre : isbn=%s, nbPages=%s, editeur=%s, auteur=%s", isbn, nbPages, editeur, auteur);
    }
}
