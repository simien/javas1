package fr.scarex.biblio;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import fr.scarex.biblio.exception.EmpruntException;
import fr.scarex.biblio.interfaces.IEmpruntable;
import fr.scarex.biblio.interfaces.INotifiable;

/**
 * @author SCAREX
 * 
 */
public class CatalogueBibliotheque extends ArrayList<DocBibliotheque>
{
    private static final long serialVersionUID = 2698927958447376020L;

    public void show() {
        for (int i = 0; i < this.size(); i++) {
            System.out.println(i + " - " + this.get(i).getTitre());
        }
    }

    @Override
    public DocBibliotheque remove(int index) {
        if (this.get(index).getEmprunteur() != null) this.get(index).getEmprunteur().decrEmprunts();
        return super.remove(index);
    }

    @Override
    public boolean remove(Object o) {
        if (((DocBibliotheque) o).getEmprunteur() != null) ((DocBibliotheque) o).getEmprunteur().decrEmprunts();
        return super.remove(o);
    }

    public boolean emprunt(int i, MembreBibliotheque m) {
        try {
            if (m.peutEmprunterAutreDocument() && this.get(i) instanceof IEmpruntable && ((IEmpruntable) this.get(i)).emprunt(m)) {
                m.incrEmprunts();
                return true;
            }
        } catch (EmpruntException e) {}
        return false;
    }

    public boolean reserver(int i, INotifiable m) {
        return this.get(i) instanceof IEmpruntable && ((IEmpruntable) this.get(i)).reserver(m);
    }

    public boolean retour(int i) {
        if (this.get(i).getEmprunteur() != null) this.get(i).getEmprunteur().decrEmprunts();
        return this.get(i) instanceof IEmpruntable && ((IEmpruntable) this.get(i)).retour();
    }

    public boolean annuler(int i, MembreBibliotheque m) {
        return this.get(i) instanceof IEmpruntable && ((IEmpruntable) this.get(i)).annulerReservation(m);
    }

    public boolean remiseEtagere(int i) {
        return this.get(i).remiseEtagere();
    }

    public void addLivre(String codeArchivage, String titre, String auteur, int annee, String isbn, int nbPages, String editeur) {
        this.add(new Livre(codeArchivage, titre, auteur, annee, isbn, nbPages, editeur));
    }

    public void addCD(String artiste, String codeArchivage, String titre, int annee, List<String> morceaux) {
        this.add(new CD(artiste, codeArchivage, titre, annee, morceaux));
    }

    public void addDocURL(String codeArchivage, String titre, int annee, String auteur, URL url, String desc) {
        this.add(new DocURL(codeArchivage, titre, annee, auteur, url, desc));
    }

    public int compteLivres() {
        int i = 0;
        for (DocBibliotheque d : this) {
            if (d instanceof Livre) i++;
        }
        return i;
    }

    public int compteCDs() {
        int i = 0;
        for (DocBibliotheque d : this) {
            if (d instanceof CD) i++;
        }
        return i;
    }
}
