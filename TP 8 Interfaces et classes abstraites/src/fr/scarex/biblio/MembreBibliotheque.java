package fr.scarex.biblio;

import fr.scarex.biblio.interfaces.INotifiable;

/**
 * @author SCAREX
 * 
 */
public abstract class MembreBibliotheque implements INotifiable
{
    private static int dernierNumeroAbonne = 0;
    protected final int numeroAbonne;
    protected String nom;
    protected String prenom;
    protected int tel;
    protected String adresse;
    protected int nbEmprunts;

    public MembreBibliotheque(String nom, String prenom, int tel, String adresse) {
        this.nom = nom;
        this.prenom = prenom;
        this.tel = tel;
        this.adresse = adresse;
        this.numeroAbonne = dernierNumeroAbonne++;
    }

    /**
     * @return the nbEmprunts
     */
    public int getNbEmprunts() {
        return nbEmprunts;
    }

    public int incrEmprunts() {
        return nbEmprunts++;
    }

    public int decrEmprunts() {
        return nbEmprunts--;
    }

    public abstract boolean peutEmprunterAutreDocument();

    @Override
    public String toString() {
        return String.format("MembreBibliotheque [nom=%s, prenom=%s, tel=%s, adresse=%s, numeroAbonne=%s]", nom, prenom, tel, adresse, numeroAbonne);
    }

    public String toScreenString() {
        return String.format("nom=%s, prenom=%s, tel=%s, adresse=%s, numeroAbonne=%s", nom, prenom, tel, adresse, numeroAbonne);
    }
}
