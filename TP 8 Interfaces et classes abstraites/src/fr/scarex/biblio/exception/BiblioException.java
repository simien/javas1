package fr.scarex.biblio.exception;

/**
 * @author SCAREX
 * 
 */
public class BiblioException extends RuntimeException
{
    private static final long serialVersionUID = -6920458686887887358L;

    public BiblioException() {}

    public BiblioException(String message) {
        super(message);
    }

    public BiblioException(Throwable cause) {
        super(cause);
    }

    public BiblioException(String message, Throwable cause) {
        super(message, cause);
    }

    public BiblioException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
