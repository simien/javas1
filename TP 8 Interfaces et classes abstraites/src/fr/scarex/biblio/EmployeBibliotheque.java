package fr.scarex.biblio;

import java.util.ArrayList;
import java.util.List;

import fr.scarex.biblio.DocBibliotheque.Statut;
import fr.scarex.biblio.interfaces.INotifiable;

/**
 * @author SCAREX
 * 
 */
public class EmployeBibliotheque implements INotifiable
{
    public static List<DocBibliotheque> docUtilises = new ArrayList<>();
    private String id;
    private String nom;
    
    public EmployeBibliotheque(String id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public void docDisponible(DocBibliotheque d) {
        TestBibliotheque.docs.remove(d);
        docUtilises.add(d);
        d.setStatut(Statut.UTILISE);
    }

    @Override
    public String toString() {
        return String.format("EmployeBibliotheque : id=%s, nom=%s", id, nom);
    }
}
