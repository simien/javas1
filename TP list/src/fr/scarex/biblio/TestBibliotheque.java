package fr.scarex.biblio;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author SCAREX
 * 
 */
public class TestBibliotheque
{
    public static CatalogueBibliotheque docs = new CatalogueBibliotheque();
    public static List<MembreBibliotheque> membres = new ArrayList<>();
    private static Scanner sc;

    public static void main(String[] args) {
        docs.add("004.178.K20PM", "Introduction à Java", "J. Leblanc", 2015);
        docs.add("967.4987.T2480", "Structure de Données", "M. Machin", 2017);

        sc = new Scanner(System.in);

        mainMenu();
    }

    public static void mainMenu() {
        System.out.println("----------------");
        System.out.println("1 - Documents");
        System.out.println("2 - Membres");
        System.out.println("3 - État bibliothèque");

        int k = sc.nextInt();
        sc.nextLine();
        switch (k) {
        case 1:
            menuDocs();
            break;
        case 2:
            menuMembres();
            break;
        case 3:
            menuEtatBiblio();
            break;
        default:
            mainMenu();
        }
    }

    private static void menuDocs() {
        System.out.println("----------------");
        System.out.println("1 - Ajouter document");
        System.out.println("2 - Voir documents");
        System.out.println("3 - Retour");

        int k = sc.nextInt();
        sc.nextLine();
        switch (k) {
        case 1:
            menuAjouterDoc();
            break;
        case 2:
            menuListeDoc();
            break;
        case 3:
            mainMenu();
            break;
        default:
            menuDocs();
        }
    }

    private static void menuMembres() {
        System.out.println("----------------");
        System.out.println("1 - Ajouter membre");
        System.out.println("2 - Liste membres");
        System.out.println("3 - Retour");
        
        int k = sc.nextInt();
        sc.nextLine();
        switch (k) {
        case 1:
            ajouterMembre();
            break;
        case 2:
            listeMembres();
            break;
        case 3:
            mainMenu();
        default:
            menuMembres();
        }
    }

    private static void ajouterMembre() {
        System.out.println("----------------");
        System.out.println("Entrez le nom");
        String nom = sc.nextLine();

        System.out.println("Entrez le prenom");
        String pren = sc.nextLine();

        System.out.println("Entrez le numero de telephone");
        int tel = sc.nextInt();
        sc.nextLine();

        System.out.println("Entrez l'adresse");
        String adr = sc.nextLine();

        creerMembre(nom, pren, tel, adr);

        menuMembres();
    }

    private static void listeMembres() {
        int k = menuMembre();
        if (k != -1) {
            System.out.println("----------------");
            System.out.println("1 - Supprimer");
            System.out.println("2 - Voir emprunts");
            System.out.println("3 - Retour");
            
            int l = sc.nextInt();
            sc.nextLine();
            switch (l) {
            case 1:
                membres.remove(l);
                System.out.println("membre supprimé");
                break;
            case 2:
                menuEmprunts(k);
                break;
            case 3:
            default:
                menuMembres();
            }
        }
        menuMembres();
    }

    private static void menuEmprunts(int l) {
        System.out.println("----------------");
        MembreBibliotheque m = membres.get(l);
        for (int i = 0; i < docs.size(); i++) {
            if (docs.get(i).getEmprunteur() != null && docs.get(i).getEmprunteur().equals(m)) {
                System.out.println(docs.get(i));
            }
        }
        listeMembres();
    }

    private static void menuEtatBiblio() {
        System.out.println("----------------");
        System.out.println(DocBibliotheque.Statut.EMPRUNTE + " : " + DocBibliotheque.Statut.EMPRUNTE.getCount());
        System.out.println(DocBibliotheque.Statut.ETAGERE + " : " + DocBibliotheque.Statut.ETAGERE.getCount());
        System.out.println(DocBibliotheque.Statut.RESERVATION + " : " + DocBibliotheque.Statut.RESERVATION.getCount());
        System.out.println(DocBibliotheque.Statut.PILE_RETOUR + " : " + DocBibliotheque.Statut.PILE_RETOUR.getCount());
        System.out.println("Appuyez sur n'importe quelle touche");
        
        sc.nextLine();
        mainMenu();
    }

    /* Menu docs */
    private static void menuAjouterDoc() {
        System.out.println("----------------");
        System.out.println("Entrez le code d'archivage");
        String code = sc.nextLine();

        System.out.println("Entrez le titre");
        String titre = sc.nextLine();

        System.out.println("Entrez le nom de l'auteur");
        String aut = sc.nextLine();

        System.out.println("Entrez l'année");
        int annee = sc.nextInt();
        sc.nextLine();

        docs.add(code, titre, aut, annee);

        menuDocs();
    }

    private static void menuListeDoc() {
        System.out.println("----------------");
        for (int i = 0; i < docs.size(); i++) {
            System.out.println(i + " - " + docs.get(i).getTitre());
        }
        System.out.println();
        System.out.println("Entrez le numéro de l'ouvrage");
        int k = sc.nextInt();
        sc.nextLine();
        if (k >= 0 && k < docs.size()) {
            menuDoc(k);
        } else {
            menuDocs();
        }
    }

    private static void menuDoc(int i) {
        System.out.println("----------------");
        System.out.println(docs.get(i).toScreenString());
        System.out.println("1 - Supprimer");
        System.out.println("2 - Emprunter");
        System.out.println("3 - Reserver");
        System.out.println("4 - Annuler réservation");
        System.out.println("5 - Retourner");
        System.out.println("6 - Remettre en étagère");
        System.out.println("7 - Retour");

        int k = sc.nextInt();
        sc.nextLine();
        switch (k) {
        case 1:
            docs.remove(i);
            break;
        case 2: {
            int l = menuMembre();
            if (l != -1) if (docs.emprunt(i, membres.get(l)))
                System.out.println("document emprunté");
            else
                System.out.println("impossible d'emprunter le document");
            menuDoc(i);
            break;
        }
        case 3: {
            int l = menuMembre();
            if (l == -1)
                menuDoc(i);
            else if (docs.reserver(i, membres.get(l)))
                System.out.println("document réservé");
            else
                System.out.println("empossible de réserver le document");
            menuDoc(i);
            break;
        }
        case 4: {
            int l = menuMembre();
            if (l == -1)
                menuDoc(i);
            else if (docs.annuler(i, membres.get(l)))
                System.out.println("document annulé");
            else
                System.out.println("empossible d'annuler la réservation");
            menuDoc(i);
            break;
        }
        case 5:
            docs.retour(i);
            break;
        case 6:
            docs.remiseEtagere(i);
            break;
        case 7:
        default:
            menuDocs();
        }
    }

    private static int menuMembre() {
        System.out.println("----------------");
        for (int i = 0; i < membres.size(); i++) {
            System.out.println(i + " - " + membres.get(i));
        }

        int k = sc.nextInt();
        sc.nextLine();
        return k >= 0 && k < membres.size() ? k : -1;
    }

    public static void creerMembre(String nom, String prenom, int tel, String adresse) {
        membres.add(new MembreBibliotheque(nom, prenom, tel, adresse));
    }
}
